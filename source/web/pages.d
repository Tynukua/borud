module web.pages;

import vibe.d;

import web : getHeaderInit, HeaderInfo, PreviewBox, PostInfo, SearchPanel;

class PagesInterface
{
    import db;

    BorudDatabase bd;

    this(BorudDatabase _bd)
    {
        bd = _bd;
    }

    void index(HTTPServerRequest req)
    {
        import std.conv : to;
        import std.array : split, array, join;
        import std.algorithm : map;

        mixin(getHeaderInit);
        int limit = ("limit" in req.query ? req.query["limit"].to!int : 20);
        int page = ("page" in req.query ? req.query["page"].to!int : 1);
        string[] tags = ("tags" in req.query ? req.query["tags"].split : []);

        auto idList = bd.searchByTags(tags, page - 1, limit).array;
        PreviewBox previewBox;
        previewBox.list = getPreviews(idList, req.queryString);
        string[] params = ["page=%s"];
        foreach (string p; ["limit", "tags"])
        {
            if (p in req.query)
                params ~= p ~ "=" ~ req.query[p];
        }
        previewBox.navigator = PreviewBox.Navigator(
            "/?" ~ params.join("&"),
            ["<<", "<", ">", ">>"],
            [1, page - 1, page + 1, cast(int) bd.getByTagsCount(tags, limit)]
        );
        auto searchPanel = SearchPanel.create(
                bd.getTagList(40).map!"a.name".array, tags);

        render!("posts.dt", headerInfo, previewBox, searchPanel);
    }

    @path("/upload")
    void getUpload(HTTPServerRequest req, HTTPServerResponse res)
    {
        import std : Nullable;

        int limit = ("limit" in req.query ? req.query["limit"].to!int : 20);
        int page = ("page" in req.query ? req.query["page"].to!int : 1);
        PreviewBox previewBox;
        Nullable!BsonObjectID userId;
        if ("userId" in req.query)
            userId = BsonObjectID.fromString(req.query["userId"]);
        Nullable!bool isPosted;
        if ("isPosted" in req.query)
            isPosted = req.query["isPosted"].to!bool;
        previewBox.list = bd.searchUploads(isPosted, userId, page - 1, limit)
            .getPreviews;
        string[] params = ["page=%s"];
        foreach (string p; ["limit", "isPosted", "userId"])
        {
            if (p in req.query)
                params ~= p ~ "=" ~ req.query[p];
        }
        previewBox.navigator = PreviewBox.Navigator(
            "/?" ~ params.join("&"),
            ["<<", "<", ">", ">>"],
            [
            1, page - 1, page + 1,
            cast(int) bd.getUploadsCount(isPosted, userId, limit)
        ]
        );

        mixin(getHeaderInit);
        res.render!("upload.dt", headerInfo, previewBox);
    }

    @path("/upload/:id")
    void getUploadById(HTTPServerRequest req, HTTPServerResponse res)
    {
        mixin(getHeaderInit);
        string ids = req.params["id"];
        auto id = BsonObjectID.fromString(ids);
        auto upload = bd.getUpload(id);
        auto postedit = PostInfo(ids, upload.mimetype, "/file/" ~ ids, "",
            upload.posted, "createPost");
        res.render!("postedit.dt", headerInfo, postedit);
    }

    @path("/editpost/:id")
    void getEditPost(HTTPServerRequest req, HTTPServerResponse res)
    {
        string ids = req.params["id"];
        mixin(getHeaderInit);
        auto post = bd.getPost(BsonObjectID.fromString(ids));
        auto upload = bd.getUpload(post.uploadId);
        auto postedit = PostInfo(ids, upload.mimetype, 
                "/file/" ~post.uploadId.toString, 
                bd.getTagList(post.tags).join(" "), false, "updatePost");
        res.render!("postedit.dt", headerInfo, postedit);
    }

    @path("/post/:id")
    void getPost(HTTPServerRequest req, HTTPServerResponse res)
    {
        import std.array : split;

        mixin(getHeaderInit);
        string ids = req.params["id"];
        string[] tags;
        if ("tags" in req.query)
            tags = req.query["tags"].split;
        auto post = bd.getPost(BsonObjectID.fromString(ids));
        auto mimetype = bd.getUpload(post.uploadId).mimetype;
        auto postInfo = PostInfo(ids, mimetype,
            "/file/" ~ post.uploadId.toString, "", false, "");
        auto searchPanel = SearchPanel.create(bd.getTagList(post.tags), tags);
        res.render!("post.dt", headerInfo, postInfo, searchPanel);
    }

}


import std.range : isInputRange, ElementType;

auto getPreviews(PreviewRange)(PreviewRange previews, string qparams = "")
in (isInputRange!PreviewRange)
{
    import db : PostId, UploadId;
    import std : map, array;

    static if (is(PostId == ElementType!PreviewRange))
        return previews.map!(post => PreviewBox.Preview(
                "/post/" ~ post._id.toString ~ (qparams ? "?" ~ qparams : ""),
                "/thumb/" ~ post.uploadId.toString)
        ).array;
    else static if (is(UploadId == ElementType!PreviewRange))
        return previews.map!(upload => PreviewBox.Preview(
                "/upload/" ~ upload._id.toString,
                "/thumb/" ~ upload._id.toString)
        ).array;
    else
        static assert(0, "Preview range wrong");
}
