module web;

public import web.auth : AuthInterface;
public import web.pages : PagesInterface;
public import web.delivery : DeliveryInterface;

struct HeaderInfo
{
    string username;
}

struct PreviewBox
{
    struct Preview
    {
        string url;
        string preview;
    }

    struct Navigator
    {
        string baseurl;
        string[] pages;
        int[] pageNumbers;
    }
    Preview[] list;
    Navigator navigator;
}

struct PostInfo
{
    string id;
    string mimetype;
    string source;
    string tags;
    bool uploadPosted;
    string formAction;
}

struct SearchPanel
{
    struct Tag
    {
        string name;
        string url;
        string urlPlus;
        string urlInfo;
    }

    string searchedText;
    Tag[] tags;

    static auto create(string[] tags, string[] searchedTags = [])
    {
        import std.algorithm : map;
        import std.array : array, join;

        SearchPanel sp;
        sp.searchedText = searchedTags.join(" ");
        sp.tags = tags.map!(
            tagName =>
                SearchPanel.Tag(
                    tagName,
                    "/?tags=" ~ tagName,
                    "/?tags=" ~ sp.searchedText ~ " " ~ tagName,
                    "/tag/" ~ tagName
                ))
            .array;
        return sp;
    }
}

string getHeaderInit()
{
    return q{
    HeaderInfo headerInfo;
    if (req.session)
        headerInfo.username = req.session.get!string("username");};
}
