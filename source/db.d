module db;

import vibe.d;
import std : Nullable;

mixin template DataMixin(T)
{
    import std.traits : Fields, FieldNameTuple;

    static foreach (i, v; FieldNameTuple!T)
    {
        mixin("Fields!T[" ~ i.stringof ~ "] " ~ v ~ ";");
    }
}

struct ActionInfo
{
    BsonObjectID userId;
    SysTime datetime;

    this(BsonObjectID userId)
    {
        this.userId = userId;
        this.datetime = Clock.currTime(UTC());
    }
}

struct UploadData
{
    string mimetype;
    ubyte[] data;
    ubyte[] thumb;
    bool posted = false;
    BsonObjectID postedId = BsonObjectID.init;
    ActionInfo actionInfo;
}

struct UploadId
{
    BsonObjectID _id;
    mixin DataMixin!UploadData;
}

struct PostData
{
    BsonObjectID uploadId;
    BsonObjectID[] tags;
    ActionInfo actionInfo;
}

struct PostId
{
    BsonObjectID _id;
    mixin DataMixin!PostData;
}

struct UserData
{
    string username;
    ubyte[] hash;
}

struct UserId
{
    BsonObjectID _id;
    mixin DataMixin!UserData;
}

struct TagData
{
    string name;
    ulong count;
    string description;
}

struct TagId
{
    BsonObjectID _id;
    mixin DataMixin!TagData;
}

class BorudDatabase
{
    MongoClient mongoClient;

    this(string connectionString)
    {
        // Initialize the MongoDB client and connect to the database
        mongoClient = connectMongoDB(connectionString);

    }

    auto uploadsCollection() @property
    {
        return mongoClient.getCollection("borud.uploads");
    }

    auto postsCollection() @property
    {
        return mongoClient.getCollection("borud.posts");
    }

    auto usersCollection() @property
    {
        return mongoClient.getCollection("borud.users");
    }

    auto tagsCollection() @property
    {
        return mongoClient.getCollection("borud.tags");
    }

    BsonObjectID createNewUpload(UploadData upload)
    {
        return uploadsCollection.insertOne(upload).insertedId;
    }

    UploadData getUpload(BsonObjectID uploadId)
    {
        auto upload = uploadsCollection.findOne!(UploadData)(["_id": uploadId]);
        enforceHTTP(!upload.isNull, HTTPStatus.notFound);
        return upload.get;
    }

    BsonObjectID submitPost(PostData post)
    {
        auto postId = postsCollection.insertOne(post).insertedId;
        uploadsCollection.updateOne(["_id": post.uploadId], [
            "$set": ["posted": true.Bson, "postedId": postId.Bson]
        ]);
        return postId;
    }

    auto updatePost(BsonObjectID id, string[] tags)
    {
        import std : map, array;

        auto post = postsCollection.findOne!PostId(["_id": id]);
        enforceHTTP(!post.isNull, HTTPStatus.notFound);
        foreach (tag; post.get.tags)
        {
            rmPostTag(id, tag);
        }
        foreach (tag; tags)
        {
            addPostTag(id, tag);
        }
    }

    PostId getPost(BsonObjectID postId)
    {
        auto post = postsCollection.findOne!(PostId)(["_id": postId]);
        enforceHTTP(!post.isNull, HTTPStatus.notFound);
        return post.get;
    }

    auto searchByTags(string[] tags, int page = 0, int count = 20)
    {
        auto q = Bson.emptyObject;
        if (tags.length)
            q["tags"] = ["$all": tags].serializeToBson;
        logDiagnostic("%s", q);
        return postsCollection.find!(PostId)(q)
            .sort(["_id": -1]).skip(page * count).limit(count);
    }

    ulong getByTagsCount(string[] tags, int count = 20)
    {
        auto q = Bson.emptyObject;
        if (tags.length)
            q["tags"] = ["$all": tags].serializeToBson;
        auto docCount = postsCollection.count(q);
        return docCount / count + (docCount % count ? 1 : 0);
    }

    auto searchUploads(
        Nullable!bool isPosted,
        Nullable!BsonObjectID userId,
        int page = 0,
        int count = 20)
    {
        auto q = Bson.emptyObject;
        if (!isPosted.isNull)
            q["posted"] = isPosted.get;
        if (!userId.isNull)
            q["actionInfo.userId"] = userId.get;
        logDiagnostic("%s", q);
        return uploadsCollection.find!(UploadId)(q)
            .sort(["_id": -1]).skip(page * count).limit(count);
    }

    ulong getUploadsCount(
        Nullable!bool isPosted,
        Nullable!BsonObjectID userId,
        int count = 20)
    {
        auto q = Bson.emptyObject;
        if (!isPosted.isNull)
            q["posted"] = isPosted.get;
        if (!userId.isNull)
            q["actionInfo.userId"] = userId.get;
        auto docCount = uploadsCollection.count(q);
        return docCount / count + (docCount % count ? 1 : 0);
    }

    auto registerUser(UserData ud)
    {
        auto found = usersCollection.findOne!UserId(["username": ud.username]);
        enforceHTTP(found.isNull, HTTPStatus.conflict);
        return usersCollection.insertOne(ud).insertedId;
    }

    auto verifyUser(UserData ud)
    {
        auto found = usersCollection.findOne!UserId(["username": ud.username]);
        enforceHTTP(!found.isNull && found.get.hash == ud.hash, HTTPStatus.forbidden);
        return found.get;
    }

    private auto addPostTag(BsonObjectID postId, string tagName)
    {
        auto tag = tagsCollection.findOne!TagId(["name": tagName]);
        BsonObjectID id;
        if (tag.isNull)
        {
            id = tagsCollection.insertOne(TagData(tagName, 1, "")).insertedId;
        }
        else
        {
            auto q = [
                "_id": postId.Bson,
                "tags": ["$in": [tag.get._id.Bson].Bson].Bson
            ].Bson;
            if (!postsCollection.findOne!PostId(q).isNull)
            {
                return;
            }
            id = tag.get._id;
            tagsCollection.updateOne(["_id": tag.get._id], [
                "$inc": ["count": 1]
            ]);
        }
        postsCollection.updateOne(["_id": postId], ["$push": ["tags": id]]);
    }

    private auto rmPostTag(BsonObjectID postId, BsonObjectID tagId)
    {
        tagsCollection.updateOne(["_id": tagId], [
            "$inc": ["count": -1]
        ]);
        postsCollection.updateOne(["_id": postId], ["$pull": ["tags": tagId]]);
    }

    string[] getTagList(BsonObjectID[] tags)
    {
        import std : array, map;

        return tagsCollection.find!TagId(["_id": ["$in": tags]]).map!"a.name".array;
    }

    auto getTagList(ulong count)
    {
        import std : map, array;

        return tagsCollection.find!TagId()
            .sort(["count": -1])
            .limit(count).array;
    }
}
