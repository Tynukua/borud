module web.delivery;

import vibe.d;

class DeliveryInterface
{
    import db;

    BorudDatabase bd;

    this(BorudDatabase _bd)
    {
        bd = _bd;
    }

    @path("/thumb/:id")
    void getThumbnail(HTTPServerRequest req, HTTPServerResponse res)
    {
        auto id = BsonObjectID.fromString(req.params["id"]);
        auto upload = bd.getUpload(id);
        res.contentType = upload.mimetype;
        res.writeBody(upload.thumb);
    }

    @path("/file/:id")
    void getFile(HTTPServerRequest req, HTTPServerResponse res)
    {
        auto id = BsonObjectID.fromString(req.params["id"]);
        auto upload = bd.getUpload(id);
        res.contentType = upload.mimetype;
        res.writeBody(upload.data);
    }

    void postUpload(HTTPServerRequest req, HTTPServerResponse res)
    {
        import std.file : read;

        enforceHTTP(req.session, HTTPStatus.unauthorized);
        enforceHTTP(!req.bodyReader.empty, HTTPStatus.badRequest);
        auto filepath = req.files["file"].tempPath.toString;
        auto upload = UploadData(
                req.files["file"].headers["Content-Type"],
                cast(ubyte[]) read(filepath),
                cast(ubyte[]) read(filepath.createThumbFile),
                false,
                BsonObjectID.init,
                ActionInfo(req.session.get!BsonObjectID("userId")));

        auto id = bd.createNewUpload(upload);
        logInfo(
            "New upload (id=%s, mimetype=%s, size=%s)",
            id, upload.mimetype, upload.data.length
        );
        res.redirect("/upload/" ~ id.toString);
    }

    @path("createPost")
    void postCreatePost(HTTPServerRequest req, HTTPServerResponse res)
    {
        import std.array : split;

        enforceHTTP(req.session, HTTPStatus.unauthorized);
        auto userId = req.session.get!BsonObjectID("userId");
        auto uploadId = BsonObjectID.fromString(req.form["id"]);
        enforceHTTP(
            bd.getUpload(uploadId).actionInfo.userId == userId,
            HTTPStatus.forbidden);
        string[] tags = req.form["tags"].split;
        auto action = ActionInfo(userId);
        auto post = bd.submitPost(
            PostData(uploadId, [], action)
        );
        bd.updatePost(post, tags);
        logInfo("New post %s", post);
        logDiagnostic("New post %s %s", post, tags);
        res.redirect("/post/" ~ post.toString);
    }

    @path("updatePost")
    void postUpdatePost(HTTPServerRequest req, HTTPServerResponse res)
    {
        import std.array : split;

        enforceHTTP(req.session, HTTPStatus.unauthorized);
        auto userId = req.session.get!BsonObjectID("userId");
        auto postId = BsonObjectID.fromString(req.form["id"]);
        enforceHTTP(
            bd.getPost(postId).actionInfo.userId == userId,
            HTTPStatus.forbidden);
        string[] tags = req.form["tags"].split;
        bd.updatePost(postId, tags);
        logInfo("Post %s updated", postId);
        logDiagnostic("post %s updated %s", postId, tags);
        res.redirect("/post/" ~ postId.toString);
    }
}

string createThumbFile(string fileName)
{
    import std.process : execute;

    auto thumbName = fileName ~ ".thumb";
    execute(["ffmpegthumbnailer", "-i", fileName, "-o", thumbName, "-s", "256"]);

    return thumbName;
}
