module web.auth;

import vibe.d;
import db;
import web : HeaderInfo;

class AuthInterface
{
    BorudDatabase bd;

    this(BorudDatabase _bd)
    {
        this.bd = _bd;
    }

    void getLogin(HTTPServerRequest req)
    {
        HeaderInfo headerInfo;
        if (req.session)
            headerInfo.username = req.session.get!string("username");
        string type = "login";
        render!("signform.dt", headerInfo, type);
    }

    void getRegister(HTTPServerRequest req)
    {
        HeaderInfo headerInfo;
        if (req.session)
            headerInfo.username = req.session.get!string("username");

        string type = "register";
        render!("signform.dt", headerInfo, type);
    }

    void postLogin(HTTPServerRequest req, HTTPServerResponse res)
    {
        enforceBadRequest("username" in req.form && "password1" in req.form,
            "Missing username/password field.");

        string username = req.form["username"];
        string password = req.form["password1"];

        auto ud = UserData(username, password.passwordHash);

        auto user = bd.verifyUser(ud);

        auto session = res.startSession();
        session.set("username", username);
        session.set("userId", user._id);
        logInfo("User %s signed in", username);
        res.redirect("/");
    }

    void postRegister(HTTPServerRequest req, HTTPServerResponse res)
    {
        enforceBadRequest(
            "username" in req.form &&
                "password1" in req.form &&
                "password2" in req.form,
            "Missing username/password field.");
        enforceBadRequest(
            req.form["password1"] == req.form["password2"],
            "Passwords are not equal");

        string username = req.form["username"];
        string password = req.form["password1"];

        import std.digest.sha : sha256Of, toHexString;

        auto userId = bd.registerUser(UserData(username, password.passwordHash));

        auto session = res.startSession();
        session.set("username", username);
        session.set("userId", userId);
        logInfo("User %s signed up", username);

        res.redirect("/");
    }

    @method(HTTPMethod.GET) @path("logout")
    void logout(HTTPServerRequest req, HTTPServerResponse res)
    {
        if (req.session)
        {
            logInfo("User %s signed out", req.session.get!string("username"));
            res.terminateSession();

        }

        res.redirect("/");
    }
}

ubyte[] passwordHash(string input)
{
    import std.digest.sha;

    return sha256Of(input ~ sha256Of("BorudSalted").toHexString).dup[];
}
