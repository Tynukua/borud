module app;

import vibe.d;

import web;
import db;

void main()
{
    setLogLevel = LogLevel.debug_;
    auto router = new URLRouter;
    import std : environment;

    auto mongodburl = environment
        .get("MONGODB_URL", "mongodb://root:example@localhost:27017");
    auto redisdburl = environment.get("REDISDB_URL", "localhost");
    auto bd = new BorudDatabase(mongodburl);
    router.registerWebInterface(new PagesInterface(bd));
    router.registerWebInterface(new AuthInterface(bd));
    router.registerWebInterface(new DeliveryInterface(bd));

    auto filesettings = new HTTPFileServerSettings();
    filesettings.maxAge = 1.dur!"minutes";
    router.get(
        "/styles/*",
        serveStaticFiles(
            "public/styles/",
            new HTTPFileServerSettings("/styles")
    )
    );

    auto settings = new HTTPServerSettings;
    settings.port = 3000;
    // settings.bindAddresses = ["localhost"];

    settings.sessionStore = new RedisSessionStore(redisdburl, 0);
    settings.keepAliveTimeout = 1.dur!"minutes";

    settings.sessionIdCookie = "borud-dev.session";
    settings.maxRequestSize = ulong.max;
    auto listner = listenHTTP(settings, router);
    runApplication;
    scope (exit)
    {
        bd.mongoClient.cleanupConnections;
        listner.stopListening;
    }
}
