from alpine

workdir /opt/app
run apk add ldc dub libc-dev gcc
run apk add zlib-dev openssl-dev

copy dub.sdl ./
run dub build --build=release || echo build


copy source ./source
copy views ./views

run dub build

from alpine

workdir /opt/app

run apk add ldc-runtime openssl zlib
run apk add ffmpegthumbnailer
COPY public ./public
COPY --from=0 /opt/app/borud ./

CMD [ "/opt/app/borud" ]
